Version 1.3.3
=============

First release.

- Linux User Manual.
- Dynamic types support.
- New XML config file format.
- Bridge libraries added.
- Types libraries added.
- Redesigned classes hierarchy.
