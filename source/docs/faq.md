# FAQs

## 固件在哪里下载？

[下载与支持 -> 固件 -> 系统 -> 对应版本](http://www.neardi.com/download-LKD3568/#zhichi)。

## Neardi-3568 支持Ubuntu 18.04/22.04 系统吗？

主要维护Ubuntu20.04系统，其他系统请自行移植。

## Ubuntu20.04 默认的用户名和密码是什么？

* 用户名：`neardi`
* 密码：`lindikeji`
* 切换超级用户 `sudo -s`

## 底板原理图在哪里下载？

底板原理图和SDK一起发的下载链接，在《硬件设计文档》目录。

## 显示屏问题

### VGA不显示

* 默认版本无VGA功能，烧录VGA固件。
* [下载与支持 -> 固件 -> Ubuntu -> VGA](http://www.neardi.com/download-LKD3568/#zhichi)。
* 烧录方法请参阅[《使用USB线升级固件》](http://wiki.neardi.com/wiki/rk3568/zh_CN/docs/upgrade.html)一章。

### VGA没声音

VGA协议是不带音频的。

### LVDS不显示

* 默认版本无LVDS功能，烧录LVD固件。
* [下载与支持 -> 固件 -> Ubuntu -> LVDS](http://www.neardi.com/download-LKD3568/#zhichi)。
* 烧录方法请参阅[《使用USB线升级固件》](http://wiki.neardi.com/wiki/rk3568/zh_CN/docs/upgrade.html)一章。

### LVDS显示无菜单

* 使用xrandr命令切换到主屏。
* 通过串口或接入HDMI2显示屏输入命令切换。

## RGA如何转换颜色？

参考`SDK/external/linux-rga`源码。

## 硬编解码如何使用？

* 网盘资料Linux/Multimedia目录
* [Rockchip MPP](https://github.com/rockchip-linux/mpp)

## 交叉编译环境

* PC交叉编译环境：SDK/prebuilts/gcc/linux-x86/aarch64/目录
* 3588板子交叉编译需要安装：

```shell
sudo apt-get install gcc g++ clang make-guile build-essential
```

## 如何进入烧录模式？

* 终端执行sudo reboot loader。
* 参阅[《使用USB线升级固件》](http://wiki.neardi.com/wiki/rk3568/zh_CN/docs/upgrade.html)一章。

## 为什么分区定义比分区固件小？

* 例如`0x00020000@0x00008000(boot)`，实际编译出来的 `boot.img` 是20几M。
* @符号前是分区的大小，@符号后是分区的起始地址，括号中是分区的名字，单位都是 sector（512Bytes）
* boot 起始地址为 0x00008000 sectors （64MB）的位置，大小为 0x20000 sectors（64M）

## 单独烧录内核分区，提示“Boot超过Flash大小 下载失败”

点击`设备分区表`后再点击`执行`。

### ssh如何以root用户登录？

有些固件默认没有配置root密码。

```shell
sudo passwd root # 输入两次密码
```
修改 **ssh配置文件**，允许root用户登录，可以用以下命令：
```shell
sudo vim /etc/ssh/sshd_config # 编辑配置文件
PermitRootLogin yes # 将这一行改为yes
```
重启 **ssh服务**，使配置生效，可以用以下命令：
```shell
sudo service ssh restart # Ubuntu系统
sudo systemctl restart sshd # CentOS系统
```
使用 **root用户和密码**，通过ssh登录Linux系统，例如：
```shell
ssh root@192.168.1.88 # 输入密码
```

### 如何取消sudo输密码？

修改`/etc/sudoers `，`%sudo ALL=(ALL:ALL) ALL`替换为`%sudo ALL=(ALL) NOPASSWD:ALL`


### 如何取消串口自动登录？

修改`/lib/systemd/system/serial-getty\@.service`，注释`autologin`这一行，打开下一行

```shell
neardi@LPA3568:/lib/systemd/system/
--- a/serial-getty@.service
+++ b/serial-getty@.service
@@ -31,8 +31,8 @@ Before=rescue.service
 # The '-o' option value tells agetty to replace 'login' arguments with an
 # option to preserve environment (-p), followed by '--' for safety, and then
 # the entered username.
-ExecStart=-/sbin/agetty --autologin root --noclear %I $TER
-#ExecStart=-/sbin/agetty -o '-p -- \\u' --keep-baud 115200,38400,9600 %I $TERM
+#ExecStart=-/sbin/agetty --autologin root --noclear %I $TER
+ExecStart=-/sbin/agetty -o '-p -- \\u' --keep-baud 115200,38400,9600 %I $TERM
 Type=idle
 Restart=always
 UtmpIdentifier=%I
```

### Usb串口无法使用？

进入配置界面，可以用以下命令：

```shell
ARCH=arm64 make menuconfig
```
* 在配置界面中，选择`Device Drivers`->`USB support`->`USB Serial Converter support`->`对应型号`，按空格选中配置。

* 保存内核配置选项菜单：

```shell
ARCH=arm64 make savedefconfig
```
* 保存后SDK根目录会生成`defconfig`文件

* 替换config：

```shell
cp defconfig kernel/arch/arm64/configs/rockchip_linux_defconfig
```

* 接着在 `kernel` 目录下编译内核：
```shell
./build.sh kernel
```
编译出来的boot.img在kernel目录，可以单独烧录分区，烧录方法请参阅[《使用USB线升级固件》](http://wiki.neardi.com/wiki/rk3568/zh_CN/docs/upgrade.html)一章。
也可以打成整包烧录。

## 如何安装OpenCV包？

```shell
sudo apt update
sudo apt install libopencv-dev python3-opencv
```

## 如何播放RTSP流？

```shell
gst-launch-1.0 rtspsrc location=rtsp://your_rtsp_url ! rtph264depay ! h264parse ! mppvideodec ! fpsdisplaysink
```

## 如何关闭屏保？

点击左下角菜单 **Preferences** -> **Screensaver** -> **Mode** -> **Disable Screen Saver** 。

## ssh经常断开连接

检查是否休眠了。

## apt update更新失败

* 检查你的网络是否正常，是否能访问外网，是否需要代理或认证。
* 更换你的软件源为国内的镜像站点，如阿里云、清华大学等，可以提高下载速度和稳定性。编辑 /etc/apt/sources.list 文件。
清除缓存和旧的索引文件，可以用以下命令：

```shell
sudo apt clean
sudo rm -rf /var/lib/apt/lists/*
```

例如将所有的 `archive.ubuntu.com` 替换为 `mirrors.tuna.tsinghua.edu.cn` ，可以用以下命令：

```shell
sudo sed -i 's/archive.ubuntu.com/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list
```

同步系统时间和时区，可以用以下命令：
```shell
sudo ntpdate cn.pool.ntp.org # 同步时间
sudo dpkg-reconfigure tzdata # 选择时区
```
应用网络更改，如果你修改了IP地址或DNS等设置，可以用以下命令：
```shell
sudo netplan apply
```

## 开机异常并循环重启怎么办？

有可能是电源电流不够，请使用电压为 12V，电流为 2.5A~3A 的电源。

## 3.0Host口插入U盘鼠标没有反应

### 临时修改的方式

强制切换为 Host 模式

```shell
sudo -s
echo host > /sys/devices/platform/fe8a0000.usb2-phy/otg_mode
```

## Ubuntu如何修改NTP服务器

Ubuntu 18.04/20.04 默认使用systemd-timesyncd服务来同步时间，它会连接到ntp.ubuntu.com这个时间服务器。若修改这个服务器，可以按照以下步骤：

1. 停止systemd-timesyncd服务，输入命令：
```shell
sudo systemctl stop systemd-timesyncd
```

2. 编辑`/etc/systemd/timesyncd.conf`文件，找到`NTP=`这一行，把`ntp.ubuntu.com`改成你想要的时间服务器，比如`cn.pool.ntp.org`。如果这一行前面有`#`符号，要去掉它。保存并退出文件。

3. 重新启动systemd-timesyncd服务，输入命令：

```shell
sudo systemctl start systemd-timesyncd
```

4.检查时间同步状态，输入命令：
```shell
timedatectl status
```

输出：

```shell
Local time: Wed 2022-12-15 16:32:19 CST
Universal time: Wed 2022-12-15 08:32:19 UTC
RTC time: Wed 2022-12-15 08:32:19
Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: yes
systemd-timesyncd.service active: yes
RTC in local TZ: no
```

5. 查询当前使用的NTP服务器：

```shell
systemctl status systemd-timesyncd.service
```

## Ubuntu 20.04 如何修改DNS

Ubuntu 20.04使用`systemd-resolved`作为本地`DNS`服务器，它会自动编辑`/etc/resolv.conf`文件，使其指向`127.0.0.53`。

1. 若修改DNS服务器，可以编辑`/etc/systemd/resolved.conf`文件，并在[Resolve]部分添加`DNS`和`FallbackDNS`参数，然后重启`systemd-resolved`服务。例如：

```shell
[Resolve] 
DNS=1.1.1.1 8.8.8.8
FallbackDNS=8.8.4.4
```

2. 重新启动systemd-resolved服务，输入命令
```shell
sudo service systemd-resolved restart
```

4. 检查DNS状态，输入命令：
```shell
systemd-resolve --status
```

## 写号工具写入SN，MAC地址

**注意:**如果开发板进行了eMMC擦除操作，之前写入的数据也会被清除。

### Windows方式

* 安装RKDevInfoWriteTool
* [下载地址](https://pan.baidu.com/s/1BYKXw8IHlwlr7NSAUKa6mg?pwd=v00t)
* RKDevInfoWriteTool的**设置**里选中”RPMB”
* 根据需要在RKDevInfoWriteTool的**设置**里配置”SN”，”WIFI MAC”，”LAN MAC”，”BT MAC”等
* 开发板进入loader模式
* RKDevInfoWriteTool进行**写入**或者**读取**操作
具体操作可以参考RKDevInfoWriteTool安装目录下的《RKDevInfoWriteTool使用指南》PDF文档。


## Ubuntu系统没有声音，该如何处理？

* 确保显示器有外放功能
* 左下角`Menu` -> `Sound & Video/Multimedia` -> `PulseAudio Volume Control` -> `Configuration`，选择对应声源。
* 安装aplay进行测试

```shell
sudo apt-get update
sudo apt-get install alsa-utils

aplay -Dhw:0,0 test.wav //播放音频
```
