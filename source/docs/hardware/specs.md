# 产品说明

## 功能概要

* 供电方式:DC 12V/3A，支持防浪涌，防反接及防过流保护。
* 多路USB接口:对外提供1路USB3.0HOST，1路USB3.0OTG，2路USB2.0 接口，板内 3 路 USB3.0 到 mini-PCIe 接口。
* 板载3个mipiPCIe接口，可以接4G/5G通讯模块和我司RK1808AI计算卡。
* 多种显示屏接口:VGA，HDMI1.4，HDMI2.0，双通道LVDS，可支持多屏异显
* 板载2路千兆以太网、BT5.0，双频WIFI6，支持802.11a/b/g/n/ac/ax协议
* 板载M.2M-KEY接口，支持外接NVMe协议SSD
* 支持3路UART接口和1路I2C接口
* 支持扩展 2 路 CANBUS 接口
* 支持Android，buildroot，Debian，Ubuntu多种OS

## 产品外观

### 正面

![](../../images/rk3568_pcba_a.png)

### 背面

![](../../images/rk3568_pcba_b.png)