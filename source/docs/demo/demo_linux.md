# Linux 工具

## [写号工具](https://pan.baidu.com/s/1gX4G6-JjAZxwlJBereIccw?pwd=bdrh)

1.rk_provision_tool移动正向测试工具。


## [PCBA测试工具](https://pan.baidu.com/s/1IoYm-nIDANoh5UXGH7XZcQ?pwd=dl2q)

1.使用该工具可以在量产阶段快速识别板端硬件中各个部件功能的好坏。

## AI开发

1.[RKNN-Toolkit](https://github.com/rockchip-linux/rknn-toolkit)

2.[RKNN C API](https://github.com/rockchip-linux/rknpu/tree/master/rknn/rknn_api)

3.[OP支持列表](https://github.com/rockchip-linux/rknn-toolkit)

4.[RKNN-Toolkit2-1.0.0（for RK3568](https://eyun.baidu.com/s/3bqHVZrD#sharelink/path=%2F)

5.[Rock-X](https://rockchips-my.sharepoint.com/personal/addy_ke_rockchips_onmicrosoft_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Faddy%5Fke%5Frockchips%5Fonmicrosoft%5Fcom%2FDocuments%2FOthers%2FRock%2DX&ga=1)

## [MPP开发参考](https://github.com/rockchip-linux/mpp)

