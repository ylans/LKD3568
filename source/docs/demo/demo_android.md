# Android 工具

App Demo 一般内置在固件里，对于不同的设备，内置的 App Demo 会有些区别，具体以实际设备烧录的固件为准。

## [RKDevicetest](https://pan.baidu.com/s/1xZhWtzKI0TAk-HUkSfE4nA?pwd=o71f)

1.预制老化工具。

2.U盘放入引导文件，这样开机就能引导进入老化模式

预制路径：`sdk\vendor\rockchip\common\apps\RKDevicetest`


## [RKUpdateService](https://pan.baidu.com/s/17X8k7cyj5jbnIU_Mmn6Y-w?pwd=b14w)

1.预制OTA升级工具。

2.U盘放入update.zip，这样开机就能引导进入本地升级

预制路径：`sdk\vendor\rockchip\common\apps\RKUpdateService`



## [RockVideoPlayer](https://pan.baidu.com/s/10U7jQL5MpqoON3uiviorvQ?pwd=n0st)

1.预制RK播放器。

预制路径：`sdk\vendor\rockchip\common\apps\RockVideoPlayer`
