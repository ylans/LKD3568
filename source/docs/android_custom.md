# Android系统定制

## 适用范围

* RK3368 Android10
* RK3399Pro Android9
* RK3399 Android7.1/Android10
* RK356X Android11
对于其他 Android 的版本，可供参考。

## 名词解释

* `CPU_TYPE`：表示主控芯片，比如rk3368，rk3399，rk3399pro, rk356x
* `PRODUCT_TYPE`：表示产品类型，比如LKD3368

## 开机 logo 动画修改

### 第一阶段

#### 代码修改的方式

* 准备 logo
    * 制作新的 logo.bmp
        * 图片属性和默认的 logo.bmp 一致，否则会出现颠倒异常
        * width，height 都为偶数
* 替换 logo
    * 替换`kernel/logo.bmp`
### 第二阶段

#### 临时修改的方式

* 准备 bootanimation.zip
    * 准备 png 文件
        * width，height 都为偶数
准备 desc.txt 文件，比如

```shell
800 1280 30
p 0 0 part0
```
        * `800 1280 30`含义：前两个数字代表图片的像素宽度和高度，30 代表帧数，也就是1秒播放的图片张数
        * `p 0 0 part0`含义：p 代表标志位，0 表示无限循环，0 表示阶段间隔时间为0，part0 表示对应文件夹
        * desc.txt 文本格式：Unix+UTF-8
    * 制作 bootanimation.zip
        * 以**存储方式**压缩成zip文件
push bootanimation.zip

```shell
adb shell setprop persist.sys.root_access 3
adb root
adb remount
adb push bootanimation.zip system/media/bootanimation.zip
adb shell reboot
```
#### 代码修改的方式

* 准备 bootanimation.zip
    * 把制作好的`bootanimation.zip`放到`device/rockchip/CPU_TYPE/`目录下
在`device/rockchip/CPU_TYPE/device.mk`文件里，添加如下内容：

```shell
PRODUCT_COPY_FILES += \
       device/rockchip/CPU_TYPE/bootanimation.zip:/system/media/bootanimation.zip
```
## 默认屏幕方向修改

### 临时修改的方式

系统可读可写

```shell
adb shell setprop persist.sys.root_access 3
adb root
adb remount
```
* 修改或添加属性值
    * Android7.1
        * 在`/system/build.prop`文件中，修改或添加`ro.sf.hwrotation`的值
            * 0：横屏
            * 90：竖屏
            * 180：反向横屏
            * 270：反向竖屏
    * Android10及以上
        * 在`vendor/build.prop`文件中，修改或添加`ro.surface_flinger.primary_display_orientation`的值
            * ORIENTATION_0：横屏
            * ORIENTATION_90：竖屏
            * ORIENTATION_180：反向横屏
            * ORIENTATION_270：反向竖屏

系统重启

```shell
adb shell reboot
```
### 代码修改的方式

Android 7.1:

* 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/system.prop`文件里，修改`ro.sf.hwrotation`的值
Android 10及以上:

* 在`device/rockchip/CPU_TYPE/BoardConfig.mk`文件里，修改`SF_PRIMARY_DISPLAY_ORIENTATION`的值
    * 0：横屏
    * 90：竖屏
    * 180：反向横屏
    * 270：反向竖屏
删除out目录下的 build.prop

```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```
## 默认隐藏状态栏

### 临时修改的方式

在 `adb shell` 模式下执行
```shell
settings put global policy_control immersive.status=*
```

### 代码修改的方式

修改内容如下

```shell
diff --git a/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml b/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml
index 2092e98d16..59c3ba0321 100644
--- a/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml
+++ b/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml
@@ -30,7 +30,10 @@

     <!-- Whether a software navigation bar should be shown. NOTE: in the future this may be
          autodetected from the Configuration. -->
     <bool name="config_showNavigationBar">true</bool>
+
+    <!-- Height of the status bar -->
+    <dimen name="status_bar_height">0dp</dimen>

     <!--  Maximum number of supported users -->
     <integer name="config_multiuserMaximumUsers">8</integer>
```

## 默认隐藏导航栏

### 代码修改的方式

修改内容如下

```shell
diff --git a/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml b/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml
index 2092e98d16..59c3ba0321 100644
--- a/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml
+++ b/device/rockchip/common/overlay/frameworks/base/core/res/res/values/config.xml
@@ -30,7 +30,10 @@
 
     <!-- Whether a software navigation bar should be shown. NOTE: in the future this may be
          autodetected from the Configuration. -->
-    <bool name="config_showNavigationBar">true</bool>
+    <bool name="config_showNavigationBar">false</bool>
+
     <!-- Height of the status bar -->
     <dimen name="status_bar_height">0dp</dimen>
```

## 内置 APP 的几种方式

### 代码修改的方式

可在`vendor`目录下新增APP的文件夹

#### 不可卸载

可参考`vendor/rockchip/app`

`Android.mk`里的内容

```shell
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := neardi_sdkapi_demo
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_CERTIFICATE := platform
#LOCAL_OVERRIDES_PACKAGES := 
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
#LOCAL_REQUIRED_MODULES :=
#LOCAL_PREBUILT_JNI_LIBS :=
include $(BUILD_PREBUILT)
```
    * LOCAL_CERTIFICATE := platform含义：APK签名使用系统签名
    * LOCAL_PRIVILEGED_MODULE如果不设置或者设置为false，安装位置为/system/app

```
#### 可卸载

* 在 device/rockchip/CPU_TYPE/PRODUCT_TYPE/ 目录下新建一个 preinstall_del 目录
* 把 apk 拷贝到 device/rockchip/CPU_TYPE/PRODUCT_TYPE/preinstall_del 目录下
* 编译 android
```
删除内置应用

### 临时修改的方式

```shell
adb shell setprop persist.sys.root_access 3
adb root
adb remount
adb shell rm -r system/app/APK_NAME/
adb shell reboot
```
或者
```shell
adb shell setprop persist.sys.root_access 3
adb root
adb remount
adb shell rm -r system/priv-app/APK_NAME/
adb shell reboot
```
### 代码修改的方式

* 如果应用有源代码，在应用源代码的`Android.mk`文件里注释掉`include $(BUILD_PACKAGE)`
* 如果应用是 APK 文件且有`Android.mk`，则在`Android.mk`文件里注释掉`include $(BUILD_PREBUILT)`
## 开机启动应用

### 修改代码的方式

#### 非 Launcher

在应用源码里的`AndroidManifest.xml`添加如下内容

```shell
package="com.example.testfile"
android:sharedUserId="android.uid.system">
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
<receiver android:name="com.example.testfile.BootBroadcastReceiver" >
    <intent-filter>
        <action android:name="android.intent.action.BOOT_COMPLETED" />
        <category android:name="android.intent.category.LAUNCHER" />
   </intent-filter>
</receiver>
```
增加广播接收的代码`BootBroadcastReceiver.java`，比如
```shell
package com.example.testfile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class BootBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "sjft";

    public static final String EXTRA_VOLUME_STATE = "android.os.storage.extra.VOLUME_STATE";

    public static final int STATE_UNMOUNTED = 0;
    public static final int STATE_CHECKING = 1;
    public static final int STATE_MOUNTED = 2;
    public static final int STATE_MOUNTED_READ_ONLY = 3;
    public static final int STATE_FORMATTING = 4;
    public static final int STATE_EJECTING = 5;
    public static final int STATE_UNMOUNTABLE = 6;
    public static final int STATE_REMOVED = 7;
    public static final int STATE_BAD_REMOVAL = 8;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        if (action.equals("android.intent.action.PACKAGE_REPLACED")){
            String packageName = intent.getData().getSchemeSpecificPart();
            Log.v(TAG,"BootBroadcastReceiver packageName:"+packageName);
            if(context.getPackageName().equals(packageName)){
                Intent launchIntent = new Intent(context, MainActivity.class);//重新启动应用
                //此处如果不想写死启动的Activity，也可以通过如下方法获取默认的启动Activity
                //Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(launchIntent);
            }
        } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            Intent launchIntent = new Intent(context, MainActivity.class);//重新启动应用
            //此处如果不想写死启动的Activity，也可以通过如下方法获取默认的启动Activity
            //Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(launchIntent);
        }
    }
}
```
* 开机自启动 apk 需要系统签名（[系统签名文件](https://pan.baidu.com/s/19tQ0Wh-mLKyHqSOf0-BP2w?pwd=31v8)）
* 必须先打开一次开机自启动 apk，才能接收到开机完成的广播
#### Launcher

* 删除或者不编译 SDK 里的 Launcher 应用
在应用源码里的`AndroidManifest.xml`里最先启动的 Activity 添加如下内容

```shell
<intent-filter>
    <action android:name="android.intent.action.MAIN" />
    <category android:name="android.intent.category.LAUNCHER" />
</intent-filter>
```
## 默认语言的修改

语言的支持列表可以查看`build/target/product/languages_full.mk`。

### 临时修改的方式

系统可读可写

```shell
adb shell setprop persist.sys.root_access 3
adb root
adb remount
```
* 修改`/system/build.prop`文件中`ro.product.locale`的值(Android 10及以上为: `/vendor/build.prop`)
    * en-US：英文
    * zh-CN：中文
系统重启

```shell
adb shell reboot
```
### 代码修改的方式

* 在`build/target/product/full_base.mk`里修改`PRODUCT_LOCALES`的值
中文

```shell
PRODUCT_LOCALES := zh_CN
```
删除 out 目录下的 build.prop
```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```
## 默认时区的修改

时区的支持列表可以查看`frameworks/base/packages/SettingsLib/res/xml/timezones.xml`。

### 临时修改的方式

系统可读可写

```shell
adb shell setprop persist.sys.root_access 3
adb root
adb remount
```
* 修改`/system/build.prop`文件中`persist.sys.timezone`的值(Android 10及以上为: `/vendor/build.prop`)
    * `Asia/Shanghai`：UTC+8
系统重启

```shell
adb shell reboot
```
### 代码修改的方式

* Android7.1/Android10
    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/system.prop`文件里，修改`persist.sys.timezone`的值
        * `Asia/Shanghai`：UTC+8
Android11

    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/PRODUCT_TYPE.mk`文件里添加如下内容：
```shell
PRODUCT_PROPERTY_OVERRIDES += persist.sys.timezone=Asia/Shanghai
```
删除 out 目录下的 build.prop
```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```
## 默认 ROOT

### 临时修改的方式

```shell
adb shell setprop persist.sys.root_access 3
```
### 代码修改的方式

Android7.1/Android10.0

    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/system.prop`文件里，添加如下内容：
```shell
persist.sys.root_access=3
```
Android11
    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/PRODUCT_TYPE.mk`文件里添加如下内容：
```shell
PRODUCT_PROPERTY_OVERRIDES += persist.sys.root_access=3
```
删除 out 目录下的 build.prop
```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```
## 默认系统时间24小时制

### 代码修改的方式

```shell
diff --git a/frameworks/base/packages/SettingsProvider/res/values/defaults.xml b/frameworks/base/packages/SettingsProvider/res/values/defaults.xml
index f95ecc6535..06055dba7b 100644
--- a/frameworks/base/packages/SettingsProvider/res/values/defaults.xml
+++ b/frameworks/base/packages/SettingsProvider/res/values/defaults.xml
@@ -244,4 +244,6 @@
 
     <!-- should show the screenshot button default -->
     <integer name="def_screenshot_button_show">0</integer>
+<!-- value 12/24/null 对应设置 12/24/自动-->
+    <string name="def_time_12_24" translatable="false">24</string>
 </resources>
diff --git a/frameworks/base/packages/SettingsProvider/src/com/android/providers/settings/DatabaseHelper.java b/frameworks/base/packages/SettingsProvider/src/com/android/providers/settings/DatabaseHelper.java
index b3ff9d08a8..ebdd5d3637 100644
--- a/frameworks/base/packages/SettingsProvider/src/com/android/providers/settings/DatabaseHelper.java
+++ b/frameworks/base/packages/SettingsProvider/src/com/android/providers/settings/DatabaseHelper.java
@@ -2261,6 +2261,9 @@ class DatabaseHelper extends SQLiteOpenHelper {
    private void loadSystemSettings(SQLiteDatabase db) {
                                     ......
+            
+            loadStringSetting(stmt, Settings.System.TIME_12_24,
+                    R.string.def_time_12_24);
```

## 默认永不休眠

### 代码修改的方式

```shell
diff --git a/device/rockchip/common/overlay_screenoff/frameworks/base/packages/SettingsProvider/res/values/defaults.xml b/device/rockchip/common/overlay_screenoff/frameworks/base/packages/SettingsProvider/res/values/defaults.xml
index 77499e6356..2573ace4c5 100644
--- a/device/rockchip/common/overlay_screenoff/frameworks/base/packages/SettingsProvider/res/values/defaults.xml
+++ b/device/rockchip/common/overlay_screenoff/frameworks/base/packages/SettingsProvider/res/values/defaults.xml
@@ -17,7 +17,8 @@
  */
 -->
 <resources>
-    <integer name="def_screen_off_timeout">0x7fffffff</integer>
+    <integer name="def_screen_off_timeout">2147483647</integer>
+    <bool name="def_lockscreen_disabled">true</bool>
```

## 设置时间无效

设置的系统时间早于编译时间，重启设置默认为编译时间

### 代码修改的方式

```shell
diff --git a/frameworks/base/services/core/java/com/android/server/AlarmManagerService.java b/frameworks/base/services/core/java/com/android/server/AlarmManagerService.java
index 7cdcc01bc0..10f4808ecb 100644
--- a/frameworks/base/services/core/java/com/android/server/AlarmManagerService.java
+++ b/frameworks/base/services/core/java/com/android/server/AlarmManagerService.java
@@ -1510,11 +1510,11 @@ class AlarmManagerService extends SystemService {
             final long systemBuildTime =  Long.max(
                     1000L * SystemProperties.getLong("ro.build.date.utc", -1L),
                     Long.max(Environment.getRootDirectory().lastModified(), Build.TIME));
-            if (mInjector.getCurrentTimeMillis() < systemBuildTime) {
+            /*if (mInjector.getCurrentTimeMillis() < systemBuildTime) {
                 Slog.i(TAG, "Current time only " + mInjector.getCurrentTimeMillis()
                         + ", advancing to build time " + systemBuildTime);
                 mInjector.setKernelTime(systemBuildTime);
-            }
+            }*/

             // Determine SysUI's uid
             mSystemUiUid = mInjector.getSystemUiUid();
```

## 默认打开网络 ADB

### 临时修改的方式

#### 方式一

```shell
adb shell setprop persist.internet_adb_enable 1
```
#### 方式二

* 打开“开发者选项”
打开 `ADB over network`

* `Settings`–>`Developeroptions`–>`ADB network`
### 代码修改的方式

Android7.1/Android10

    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/system.prop`文件里，添加如下内容：
```shell
persist.internet.adb.enable=1
```
Android11
    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/PRODUCT_TYPE.mk`文件里，添加如下内容：
```shell
PRODUCT_PROPERTY_OVERRIDES += persist.internet.adb.enable=1
```
删除 out 目录下的 build.prop
```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```
## 默认设置 OTG USB3.0 为 devices 模式

### 临时修改的方式

```shell
adb shell setprop persist.usb.mode 2
```
### 代码修改的方式

Android7.1/Android10

    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/system.prop`文件里，添加如下内容：
```shell
persist.usb.mode=2
```
Android11
    * 在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/PRODUCT_TYPE.mk`文件里，添加如下内容：
```shell
PRODUCT_PROPERTY_OVERRIDES += persist.usb.mode=otg
```
删除 out 目录下的 build.prop
```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```
## 打开或关闭未知应用安装功能

### 代码修改的方式

#### Android 7.1

在`frameworks/base/packages/SettingsProvider/res/values/defaults.xml`文件里，修改`def_install_non_market_apps`的值。

* false : 关闭
* true : 打开
#### Android 10及以上

在Android10中，已经删除了”允许未知来源”安装应用程序的永久授权选项，从系统设置当中已经找不到该开关，谷歌将永久授权修改为每次的单独授权，当用户每次安装第三方来源的Android软件时需要对软件权限进行手动确认。

## 打开或关闭触摸声音

### 代码修改的方式

在`frameworks/base/packages/SettingsProvider/res/values/defaults.xml`文件里，修改`def_sound_effects_enabled`的值。

* false : 关闭
* true : 打开

## 配置 USB 摄像头前置或者后置

默认是前置的。(Android7.1 and Android10)

### 临时修改的方式

前置

```shell
adb shell setprop persist.sys.uvc.facing front
```
后置
```shell
adb shell setprop persist.sys.uvc.facing back
```
### 代码修改的方式

* Android7.1/Android10
在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/system.prop`文件里，添加如下内容：

        * 前置
```shell
persist.sys.uvc.facing=front
```
        * 后置
```shell
persist.sys.uvc.facing=back
```
* Android11
在`device/rockchip/CPU_TYPE/PRODUCT_TYPE/PRODUCT_TYPE.mk`文件里，添加如下内容：

        * 前置
```shell
PRODUCT_PROPERTY_OVERRIDES += persist.sys.uvc.facing=front
```
        * 后置
```shell
PRODUCT_PROPERTY_OVERRIDES += persist.sys.uvc.facing=back
```
删除 out 目录下的 build.prop
```shell
rm out/target/product/PRODUCT_TYPE/obj/ETC/system_build_prop_intermediates/build.prop
```

 

