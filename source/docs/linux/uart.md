## UART

### 简介

Neardi-3568支持RS232、RS485接口

* RS485x1
* RS232x2
其中开发板的 RS232 接口由主控的 uart4 和 uart7 扩展出来，RS485 是由 uart9 扩展出来

### DTS配置

文件路径`kernel/arch/arm64/boot/dts/rockchip/rk3568.dtsi`

```shell
@@ -3095,8 +3095,8 @@
                reg-io-width = <4>;
                dmas = <&dmac0 8>, <&dmac0 9>;
                pinctrl-names = "default";
-               pinctrl-0 = <&uart4m0_xfer>;
-               status = "disabled";
+               pinctrl-0 = <&uart4m1_xfer>;
+               status = "okay";
        };
 
        uart5: serial@fe690000 {
@@ -3137,8 +3137,8 @@
                reg-io-width = <4>;
                dmas = <&dmac0 14>, <&dmac0 15>;
                pinctrl-names = "default";
-               pinctrl-0 = <&uart7m0_xfer>;
-               status = "disabled";
+               pinctrl-0 = <&uart7m1_xfer>;
+               status = "okay";
        };
 
        uart8: serial@fe6c0000 {
@@ -3165,8 +3165,8 @@
                reg-io-width = <4>;
                dmas = <&dmac0 18>, <&dmac0 19>;
                pinctrl-names = "default";
-               pinctrl-0 = <&uart9m0_xfer>;
-               status = "disabled";
+               pinctrl-0 = <&uart9m1_xfer>;
+               status = "okay";
        };

```
配置好串口后，硬件接口对应软件上的节点分别为：
```shell
RS232:   /dev/ttysWK0、/dev/ttysWK1、/dev/ttysWK2、/dev/ttysWK3
RS485:   /dev/ttyS9
```
### PIN 脚定义

* RS232

   ![](../../images/uart_rs232.png)

* RS485

   ![](../../images/uart_rs485.png)


```
### 调试方法

用户可以根据不同的接口使用不同的主机的 USB 转串口适配器向开发板的串口收发数据，例如 RS485 的调试步骤如下：

(1) 连接硬件

将开发板RS485 的A、B、GND 引脚分别和主机串口适配器（USB 转 485 转串口模块）的 A、B、GND 引脚相连。

(2) 打开主机的串口终端

在终端输入安装kermit命令`sudo apt install ckermit`，安装完成后打开kermit，设置波特率并连接：

```shell
$ sudo kermit
C-Kermit> set line /dev/ttyUSB0
C-Kermit> set speed 9600
C-Kermit> set flow-control none
C-Kermit> connect
```
* `/dev/ttyUSB0` 为 主机USB 转串口适配器的设备文件
(3) 开发板发送数据

开发板的RS485 设备文件为 /dev/ttyS0。在开发板设备上运行下列命令：

```shell
echo "neardi RS485 test..." > /dev/ttyS9
```
主机中的串口终端即可接收到字符串 “neardi RS485 test…”
(4) 开发板接收数据

首先在开发板设备上运行下列命令：

```shell
cat /dev/ttyS9
```
然后在主机的串口终端输入字符串 “Neardi RS485 test…”，设备端即可见到相同的字符串。
(5) 主机退出kermit串口连接

`ctrl+\`后按c，退回终端可输入exit

```shell
C-Kermit>exit
OK to exit? ok
```

 

