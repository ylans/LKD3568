## LED

### 前言

LKD3568开发板上有 2 个 LED 状态工作灯，如下表所示：


|LED|Pin name|Pin number|
|:----|:----|:----|
|Green|GPIO0_B7|15|
|Green|GPIO4_C4|148|

可通过使用 LED 设备子系统或者直接操作 GPIO 控制该 LED。

### 以设备的方式控制 LED

标准的 Linux 专门为 LED 设备定义了 LED 子系统。 在 LKD3568开发板中的两个 LED 均以设备的形式被定义。

用户可以通过 `/sys/class/leds/` 目录控制这两个 LED。

用户可以通过 `echo` 命令向其 `brightness` 属性输入命令控制每一个 LED：

```shell
echo 0 > /sys/class/leds/neardi:work1:user/brightness //灯亮
echo 1 > /sys/class/leds/neardi:work1:user/brightness //灯灭
```
### 使用 trigger 方式控制 LED

Trigger 包含多种方式可以控制 LED，这里就用两个例子来说明。

* Simple trigger LED
* Complex trigger LED
更详细的说明请参考 `leds-class.txt` 。

首先我们需要知道定义多少个 LED，同时对应的 LED 的属性是什么。

在 `kernel/arch/arm64/boot/dts/rockchip/rk3568-neardi.dtsi` 文件中定义 LED 节点，具体定义如下：

```shell
neardi_leds: leds {
	status = "okay";
	compatible = "gpio-leds";
	user_led: user {
		label = "neardi:work1:user";
		linux,default-trigger = "ir-power-click";
		default-state = "on";
		gpios = <&gpio0 RK_PB7 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&led_user>;
	};
	user_led: user {
		label = "neardi:work2:user";
		linux,default-trigger = "ir-user-click";
		default-state = "off";
		gpios = <&gpio4 RK_PC4 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&led_user>;
	};
};
```
注意：`compatible` 的值要跟 `drivers/leds/leds-gpio.c` 中的 `.compatible` 的值要保持一致。
#### Simple trigger LED

（1）定义 LED 触发器在 `kernel/drivers/leds/trigger/led-neardi-demo.c` 文件中有如下添加：

```shell
DEFINE_LED_TRIGGER(ledtrig_default_control);
```
（2）注册该触发器
```shell
led_trigger_register_simple("ir-user-click", &ledtrig_default_control);
```
（3）控制 LED 的亮。
```shell
led_trigger_event(ledtrig_default_control, LED_FULL);     #led on
```
（4）打开LED demo
led-neardi-demo 默认没有打开，如果需要的话可以使用以下补丁打开 demo 驱动：

```shell
--- a/kernel/arch/arm64/boot/dts/rockchip/rk356x-neardi-demo.dtsi
+++ b/kernel/arch/arm64/boot/dts/rockchip/rk356x-neardi-demo.dtsi
@@ -49,7 +49,7 @@
            led_demo: led_demo {
-                status = "disabled";
+                status = "okay";
                 compatible = "neardi,rk356x-led";
                 };
```
#### Complex trigger LED

如下是 trigger 方式控制 LED 复杂一点的例子，`timer trigger` 就是让 LED 工作灯不断亮灭的效果：

我们需要在内核把 timer trigger 配置上。

在 `kernel` 路径下使用 `make menuconfig`，按照如下方法将 timer trigger 驱动选中。

```shell
Device Drivers
--->LED Support
   --->LED Trigger support
      --->LED Timer Trigger
```
保存配置并编译内核，把 `kernel.img` 烧到 LDK3568板子上 我们可以使用串口输入命令，就可以看到工作灯不停的间隔闪烁。
```shell
echo "timer" > sys/class/leds/neardi\:work1\:user/trigger
```
用户还可以使用 `cat` 命令获取 trigger 的可用值：
```shell
:/  # cat sys/class/leds/neardi\:work1\:user/trigger
none rc-feedback test_ac-online test_battery-charging-or-full test_battery-charging
test_battery-full test_battery-charging-blink-full-solid test_usb-online mmc0 mmc1
ir-user-click [timer] heartbeat backlight default-on rfkill0 mmc2 rfkill1 rfkill2
```

 

