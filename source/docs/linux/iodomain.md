# 配置电压域

## 硬件配置

每个项目都必须填写这个表格确定实际硬件接法和软件配置是否匹配，务必匹配后再编译固件：

|IO domain|实际使用电压|可配置电压|电压来源|备注|
|:----|:----|:----|:----|:----|
|PMUIO1|3.3V|Fixed|    |PMU所在组|
|PMUIO2|3.3V|1.8/3.3|LDO6|    |
|VCCIO1|3.3V|1.8/3.3|LDO4/VCCIO_ACODEC|809Codec所在组|
|VCCIO2|1.8V|1.8/3.3|BUCK5|EMMC所在组|
|VCCIO3|3.3V|1.8/3.3|LDO5/VCCIO_SD|SD卡所在组|
|VCCIO4|1.8V|1.8/3.3|BUCK5|BT/WIFI所在组|
|VCCIO5|3.3V|1.8/3.3|VCC3V3_SYS|PCIE所在组|
|VCCIO6|1.8V|1.8/3.3|BUCK5|GMAC所在组|
|VCCIO7|3.3V|1.8/3.3|VCC3V3_SYS|    |

第一次编译会提示配置电压域，选项如下：

* PMUIO2:3.3V

   ![](../../images/iodomain_pmuio2.png)

* VCCIO1:3.3V

   ![](../../images/iodomain_vccio1.png)

* VCCIO3:3.3V

   ![](../../images/iodomain_vccio3.png)

* VCCIO4:1.8V

   ![](../../images/iodomain_vccio4.png)

* VCCIO5:3.3V

   ![](../../images/iodomain_vccio5.png)

* VCCIO6:1.8V

   ![](../../images/iodomain_vccio6.png)

* VCCIO7:3.3V

   ![](../../images/iodomain_vccio7.png)

## DTS配置

文件路径`kernel/arch/arm64/boot/dts/rockchip/rk3568-evb.dtsi`
``` shell
&pmu_io_domains {
        status = "okay";
        pmuio2-supply = <&vcc3v3_pmu>;
        vccio1-supply = <&vccio_acodec>;
        vccio3-supply = <&vccio_sd>;
        vccio4-supply = <&vcc_1v8>;
        vccio5-supply = <&vcc_3v3>;
        vccio6-supply = <&vcc_1v8>;
        vccio7-supply = <&vcc_3v3>;
};
```

## VCCIO说明

### VCCIO供电

* 若`VCCIO2`供电`1.8V`时，则`FLASH_VOL_SEL`管脚必须保持为高电平，芯片会根据`FLASH_VOL_SEL=Logic high`，VCCIO2自动配置为`1.8V` mode；
* 若`VCCIO2`供电`3.3V`时，则`FLASH_VOL_SEL`管脚必须保持为低电平，芯片会根据`FLASH_VOL_SEL=Logic low`，VCCIO2自动配置为`3.3V` mode。
* 若`VCCIO2`供电电电压和`FLASH_VOL_SEL`未满足以上关系，功能会异常（比如无法正常开机）或IO损坏。

### 软件电压配置

* 当硬件IO电平接`1.8V`，软件电压配置也要相应配成`1.8V`，如果误配置为`3.3V`，这个电源域的IO功能会异常；
* 当硬件IO电平接`3.3V`，软件电压配置也要相应配成`3.3V`，如果误配置为`1.8V`，会使得这个电源域IO处于过压状态，长期工作IO会损坏。

### 代码中配置

* `RK3568_AIoT_REF_SCH`参考图`VCCIO3`电源域IO默认分配为`SD Card`功能，
`SD Card`工作模式有：`SD2.0模式`和`SD3.0模式`；`SD2.0模式`的IO电平为`3.3V`，`SD3.0模式`的IO电平为`1.8V`；
* 实际工作时，默认工作在`SD2.0`模式，即`3.3V` IO，如果插入的卡支持`SD3.0`，会协商后，IO电平切成`1.8V` IO，再切成运行`SD3.0`的速率；
* 参考图`VCCIO3` IO电源由`RK809-5 LDO5`供电（网络名为`VCCIO_SD`），默认`3.3V`，根据`SD Card`工作模式，`RK3568`会通过I2C0配置`RK809-5 LDO5`输出电压。
* 举例`《rk3568-evb.dtsi》`IO domain默认配置为 `vccio3-supply = <&vccio_sd>`;
* `sdmmc0` 相关配置如下：

``` shell
&sdmmc0 {
    max-frequency = <150000000>;
    supports-sd;
    bus-width = <4>;
    cap-mmc-highspeed;
    cap-sd-highspeed;
    disable-wp;
    sd-uhs-sdr104;
    vmmc-supply = <&vcc3v3_sd>;
    vqmmc-supply = <&vccio_sd>;
    pinctrl-names = "default";
    pinctrl-0 = <&sdmmc0_bus4 &sdmmc0_clk &sdmmc0_cmd &sdmmc0_det>;
    status = "okay";
};
``` 

* 要支持完整的`SD Card`功能，按这个配置即可，软件会根据`SD Card`模式自动切IO电压以及对应的`IO domain`电压模式。
* 若不需要工作在`SD3.0模式`，只需要固定在`SD2.0模式`（`SD3.0卡`也只工作在`SD2.0模式`），比如采用分立电源时，要省掉`3.3V`切换成`1.8V`的电路，硬件上`VCCIO3 IO`电源供电会修改成`VCC_3V3`，软件需要做以下改动：
`IO domain`配置改为 `vccio3-supply = <&vcc_3v3>`;

* `sdmmc0` 相关配置修改成如下：
``` shell
&sdmmc0 {
    max-frequency = <150000000>;
    supports-sd;
    bus-width = <4>;
    cap-mmc-highspeed;
    cap-sd-highspeed;
    disable-wp;
    vmmc-supply = <&vcc3v3_sd>;
    vqmmc-supply = <&vcc_3v3>;
    pinctrl-names = "default";
    pinctrl-0 = <&sdmmc0_bus4 &sdmmc0_clk &sdmmc0_cmd &sdmmc0_det>;
    status = "okay";
};
```
* 需要去掉`sd-uhs-sdr104`，如果没去掉，会跑`SD3.0模式`，高速卡会读失败。
* 如果`VCCIO3`电源域IO分配成其它功能，比如`UART5`、`UART6`功能，那么遵守注***软件电压配置***