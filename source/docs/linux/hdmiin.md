# HDMI-IN

Neardi-3568 有一个 HDMI-IN 接口, 由 RK628芯片 转换， 支持标准的 HDMI2.0 协议，它具备以下的特性

- 支持HDMI 1.4/2.0、RGB、BT.1120等多种输入接口，最高支持4K@60fps的分辨率
- 支持HDCP 1.4、EDID、CEC等功能

   ![](../../images/rk3568_hdmiin.png)

## Android 使用 HDMI-IN

默认版本无HDMI-IN功能，烧录[Neardi-3568 HDMIIN 固件百度网盘](https://pan.baidu.com/s/1IAqbIMcCAvFB8YlH4pyFNg?pwd=ldkj)
烧录方法请参阅[《使用USB线升级固件》](http://wiki.neardi.com/wiki/rk3568/zh_CN/docs/upgrade.html)一章。

烧录完成后打开系统Camera应用，即可验证HDMI-IN功能。

## Linux 使用 HDMI-IN

Neardi-3568 HDMI-IN 默认节点是 `/dev/video1`。

依次执行以下几条命令，分辨率、format自行调节：
```shell
media-ctl -d /dev/media0 --set-v4l2 '"rkisp-isp-subdev":0[fmt:UYVY2X8/1920x1080]'
media-ctl -d /dev/media0 --set-v4l2 '"rkisp-isp-subdev":0[crop:(0,0)/1920x1080]'
v4l2-ctl -d /dev/video1 --set-selection=target=crop,top=0,left=0,width=1920,height=1080
sudo gst-launch-1.0 v4l2src device=/dev/video1 ! video/x-raw,format=NV12,width=1920,height=1080, framerate=30/1 ! fpsdisplaysink
```

执行成功返回结果：

```shell
sudo gst-launch-1.0 v4l2src device=/dev/video1 ! video/x-raw,format=NV12,width=800,height=600, framerate=30/1 ! fpsdisplaysink
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
0:00:09.1 / 99:99:99.
0:00:09.2 / 99:99:99.
0:02:31.0 / 99:99:99..
```

   ![](../../images/hdmiin-display.png)

## FAQ

### Gstreamer命令如何修改帧率

```shell
sudo gst-launch-1.0 v4l2src device=/dev/video1 ! videorate ! video/x-raw,format=NV12,width=800,height=600, framerate=60/1 ! fpsdisplaysink
<<<<
```

修改`framerate`的值，`60/1 `为60帧。

### HDMI-IN不显示

通过`v4l2-ctl`抓图验证

#### 现象一：

```shell
v4l2-ctl -d /dev/video1  --set-fmt-video=width=1920,height=1080,pixelformat=NV12  --stream-mmap=3  --stream-skip=3  --stream-to=/hdmiin.out  --stream-count=1  --stream-poll

<<<<
```
返回`<<<<`说明硬件、HDMI线缆正常，再查看拓扑结构和Format。

查看拓扑结构：

```shell
media-ctl -p -d /dev/media0
```

查看Format：

```shell
v4l2-ctl -d 1 -V
```

#### 现象二：

```shell
root@LPA3568:~# v4l2-ctl -d /dev/video1  --set-fmt-video=width=1920,height=1080,pixelformat=NV12  --stream-mmap=3  --stream-skip=3  --stream-to=/hdmiin.out  --stream-count=1  --stream-poll >/dev/null
The pixelformat 'NV24' is invalid
could not open /tmp/cif5.out for writing
            VIDIOC_STREAMON returned -1 (No such device)
```

返回`returned -1 (No such device)`检查RK628芯片有没有贴，有一版硬件移除了HDMIIN功能。

   ![](../../images/rk3568_rk628.png)


#### 现象三：

```shell
v4l2-ctl -d /dev/video1  --set-fmt-video=width=1920,height=1080,pixelformat=NV12  --stream-mmap=3  --stream-skip=3  --stream-to=/tmp/hdmiin.out  --stream-count=1  --stream-poll

select timeout
```
返回`select timeout`，检查HDMIIN线缆是否插紧或换一根线。

查看HDMIIN配置

```shell

neardi@LPA3568:~$ media-ctl -d /dev/media0 -p
Media controller API version 4.19.255

Media device information
------------------------
driver          rkisp-vir0
model           rkisp0
serial          
bus info        
hw revision     0x0
driver version  4.19.255

Device topology
- entity 1: rkisp-isp-subdev (4 pads, 7 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
   pad0: Sink
      [fmt:UYVY8_2X8/1920x1080 field:none
       crop.bounds:(0,0)/1920x1080
       crop:(0,0)/1920x1080]
       
   ...

- entity 67: rockchip-csi2-dphy0 (2 pads, 2 links)
             type V4L2 subdev subtype Unknown flags 0
             device node name /dev/v4l-subdev2
   pad0: Sink
      [fmt:UYVY8_2X8/3840x2160@10000/300000 field:none]
      <- "m00_b_rk628-csi rk628-csi":0 [ENABLED]
   pad1: Source
      [fmt:UYVY8_2X8/3840x2160@10000/300000 field:none]
      -> "rkisp-csi-subdev":0 [ENABLED]

- entity 70: m00_b_rk628-csi rk628-csi (1 pad, 1 link)
             type V4L2 subdev subtype Sensor flags 0
             device node name /dev/v4l-subdev3
   pad0: Source
      [fmt:UYVY8_2X8/3840x2160@10000/300000 field:none]
      [dv.caps:BT.656/1120 min:1x1@0 max:10000x10000@400000000 stds:CEA-861,DMT,CVT,GTF caps:interlaced,progressive,reduced-blanking,custom]
      [dv.detect:BT.656/1120 3840x2160p30 (4400x2250) stds: flags:]
      [dv.current:BT.656/1120 3840x2160p15 (4400x4490) stds: flags:]
      -> "rockchip-csi2-dphy0":0 [ENABLED]
```

其中`[dv.current:BT.656/1120 3840x2160p15 (4400x4490) stds: flags:]`为当前分辨率及帧率。

若返回`[dv.query:no-lock]`，检查HDMIIN线缆是否插紧或换一根线。
