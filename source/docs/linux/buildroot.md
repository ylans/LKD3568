# Buildroot 编译

Buildroot 是 Linux 平台上一个构建嵌入式 Linux 系统的框架。整个 Buildroot 是由 Makefile(*.mk) 脚本和 Kconfig(Config.in) 配置文件构成的。你可以和编译 Linux 内核一样，通过 buildroot 配置，menuconfig 修改，编译出一个完整的可以直接烧写到机器上运行的 Linux 系统软件（包含 boot、kernel、rootfs 以及 rootfs 中的各种库和应用程序）。若您要了解更多 Buildroot 开发相关内容，可以参考 Buildroot 官方的 [《开发手册》](https://buildroot.org/downloads/manual/manual.html)。

## 硬件配置

编译 Ubuntu 开发环境硬件配置建议：

- 64 位 CPU
- Ubuntu20.04系统
- 16GB  内存
- 250GB  空闲空间用来编译
- 普通用户编译，不要使用 root 用户编译

## 软件配置

**安装环境包**

``` shell
sudo apt update
sudo apt-get install git ssh make gcc libssl-dev liblz4-tool \
expect g++ patchelf chrpath gawk texinfo chrpath diffstat binfmt-support \
qemu-user-static live-build bison flex fakeroot cmake gcc-multilib g++-multilib \
unzip device-tree-compiler libncurses-dev

sudo pip install python-pip pyelftools
```

**SDK获取**

发送邮件至 [support@neardi.com](support@neardi.com) 获取相关开发资料

下载完成后，在解压前先查看 MD5 码和Neardi-3568-SDK-Linux-Version.tar.gz.md5sum中是否一致：

``` shell
$ md5sum Neardi-3568-SDK-Linux-Version.tar.gz
******************************** Neardi-3568-SDK-Linux-Version.tar.gz
``` 

解压：

``` shell
tar zxvf Neardi-3568-SDK-Linux-Version.tar.gz
``` 

提取：

``` shell
cd Neardi-3568-SDK-Linux-Version
git reset --hard
```

切换分支：

|branch|explain|
|:----|:----|
|LZ12000003|Neardi master branch|

``` shell
git checkout LZ12000003
``` 

## 编译 SDK

### 配置电压域

第一次编译必须填写如下表格，确定实际硬件接法和软件配置是否匹配，若不匹配会导致硬件损坏。
具体请参阅[《配置电压域》](http://wiki.neardi.com/wiki/rk3568/zh_CN/docs/linux/iodomain.html)一章。

|IO domain|实际使用电压|可配置电压|电压来源|备注|
|:----|:----|:----|:----|:----|
|PMUIO2|3.3V|1.8/3.3|LDO6|    |
|VCCIO1|3.3V|1.8/3.3|LDO4/VCCIO_ACODEC|809Codec所在组|
|VCCIO3|3.3V|1.8/3.3|LDO5/VCCIO_SD|SD卡所在组|
|VCCIO4|1.8V|1.8/3.3|BUCK5|BT/WIFI所在组|
|VCCIO5|3.3V|1.8/3.3|VCC3V3_SYS|PCIE所在组|
|VCCIO6|1.8V|1.8/3.3|BUCK5|GMAC所在组|
|VCCIO7|3.3V|1.8/3.3|VCC3V3_SYS|    |

### 选择配置

|boardConfig|explain|
|:----|:----|
|BoardConfig-rk3568-neardi-linux-lz120-f0.mk|HDMI1 + HDMI2 + HDMIIN|
|BoardConfig-rk3568-neardi-linux-lz120-f1.mk|VGA + HDMI2 + HDMIIN|
|BoardConfig-rk3568-neardi-linux-lz120-f2.mk|EV101WXM-N10 10.1inc LVDS + HDMIIN|
|BoardConfig-rk3568-neardi-linux-lz120-f3.mk|HDMI1 + HDMI2 + OV13855|

```shell
./build.sh lunch BoardConfig-rk3568-neardi-linux-lz120-f0.mk
```

注：若无`BoardConfig-rk3568-neardi-linux-lz120-f0.mk`选项，可以选择`BoardConfig-rk3568-neardi-linux-lc120.mk`。

### 全自动编译

**第一次编译SDK，务必全编一次，否则打包会出错**

全自动编译会执行所有编译, 编译生成u-boot、kernel和buildroot。

```shell
./build.sh
```
### 部分编译

* 编译 u-boot
```shell
./build.sh uboot
```

* 编译 kernel

```shell
./build.sh kernel
```

* 编译 recovery
```shell
./build.sh recovery
```

### 编译Buildroot

```shell
./build.sh rootfs
```
### 打包固件

RK 固件，是以 Rockchip 专有格式打包的固件，使用 Rockchip 提供的工具可以烧写到 eMMC 或者 SD 卡中(注：若无特殊说明，WIKI 上提及的固件默认为 RK 固件)。

```shell
# 打包 RK 固件
./build.sh updateimg
```

生成的完整固件会保存到 `rockdev/update.img`。

## 分区说明

### parameter 分区表

parameter.txt 文件中包含了固件的分区信息，以 parameter-buildroot-fit.txt 为例：

路径：`device/rockchip/rk356x/parameter-buildroot-fit.txt`

```shell
FIRMWARE_VER: 1.0
MACHINE_MODEL: RK3568
MACHINE_ID: 007
MANUFACTURER: RK3568
MAGIC: 0x5041524B
ATAG: 0x00200800
MACHINE: 0xffffffff
CHECK_MASK: 0x80
PWR_HLD: 0,0,A,0,1
TYPE: GPT
CMDLINE: mtdparts=rk29xxnand:0x00002000@0x00004000(uboot),0x00002000@0x00006000(misc),0x00020000@0x00008000(boot),0x00020000@0x00028000(recovery),0x00010000@0x00048000(backup),-@0x00058000(rootfs:grow)
uuid:rootfs=614e0000-0000-4b53-8000-1d28000054a9
```
CMDLINE 属性是我们关注的地方，以 uboot 为例， 0x00002000@0x00004000(uboot) 中 0x00004000 为uboot 分区的起始位置，0x00002000 为分区的大小，以此类推。
### package-file

package-file 文件用于打包固件时确定需要的分区镜像和镜像路径，同时它需要与 parameter.txt 文件保持一致。

路径：`tools/linux/Linux_Pack_Firmware/rockdev/package-file`

```shell
# NAME          Relative path
#
#HWDEF          HWDEF
package-file    package-file
bootloader      Image/MiniLoaderAll.bin
parameter       Image/parameter.txt
uboot           Image/uboot.img
misc            Image/misc.img
boot            Image/boot.img
recovery        Image/recovery.img
rootfs          Image/rootfs.img
userdata        RESERVED
backup          RESERVED
```

## FAQ

### 编译错误

#### dl文件下载不成功

请选择以下网盘下载 `LKD3568-Buildroot-DL.tar.gz` 并解压替换至 `buildroot/dl` 目录

- [LKD3568 Buildroot dl百度网盘](https://pan.baidu.com/s/1gk5gc9L4JqjMKpExnIxH-A?pwd=ldkj)

```shell
tar zxvf LKD3568-Buildroot-DL.tar.gz
cp -rf LKD3568-Buildroot-DL/dl LKD3568-SDK-Linux-V1.0/buildroot/
```

#### lz4错误

```shell
Incorrect parameters
Usage :
      lz4 [arg] [input] [output]

input   : a filename
          with no FILE, or when FILE is - or stdin, read standard input
Arguments :
 -1     : Fast compression (default) 
 -9     : High compression 
 -d     : decompression (default for .lz4 extension)
 -z     : force compression
 -f     : overwrite output without prompting 
 -h/-H  : display help/long help and exit
arch/arm64/boot/Makefile:31: recipe for target 'arch/arm64/boot/Image.lz4' failed
make[2]: *** [arch/arm64/boot/Image.lz4] Error 1
make[2]: *** Deleting file 'arch/arm64/boot/Image.lz4'
arch/arm64/Makefile:170: recipe for target 'Image.lz4' failed
make[1]: *** [Image.lz4] Error 2
make[1]: *** Waiting for unfinished jobs....
```

解决方法，重新手动安装lz4：

```shell
git clone https://github.com/lz4/lz4.git
cd lz4
make
sudo make install
```