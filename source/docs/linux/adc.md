## ADC

### 简介

Neardi-3568 开发板上的 AD 接口有两种，分别为：温度传感器 (Temperature Sensor)、逐次逼近ADC (Successive Approximation Register)。其中：

* TS-ADC(Temperature Sensor)：支持两通道，时钟频率必须低于800KHZ
* SAR-ADC(Successive Approximation Register)：支持六通道单端10位的SAR-ADC，时钟频率必须小于13MHZ。
内核采用工业 I/O 子系统来控制 ADC，该子系统主要为 AD 转换或者 DA 转换的传感器设计。 下面以 SAR-ADC 为例子，介绍 ADC 的基本配置方法。

### DTS配置

#### 配置DTS节点

Neardi-3568 SAR-ADC 的 DTS 节点在 `kernel/arch/arm64/boot/dts/rockchip/rk3568.dtsi` 文件中定义，如下所示：

```shell
saradc: saradc@fe720000 {
	compatible = "rockchip,rk3568-saradc", "rockchip,rk3568-saradc";
	reg = <0x0 0xfe720000 0x0 0x100>;
	interrupts = <GIC_SPI 93 IRQ_TYPE_LEVEL_HIGH>;
	#io-channel-cells = <1>;
	clocks = <&cru CLK_SARADC>, <&cru PCLK_SARADC>;
	clock-names = "saradc", "apb_pclk";
	resets = <&cru SRST_P_SARADC>;
	reset-names = "saradc-apb";
	status = "disabled";
};
```
用户首先需在 DTS 文件中添加 ADC 的资源描述：
Linux DTS: kernel/arch/arm64/boot/dts/rockchip/rk3568-neardi-android-lc120.dts

Android DTS: kernel/arch/arm64/boot/dts/rockchip/rk3568-neardi-android-lc120.dts

```shell
   adc_demo: adc_demo{
       status = "disabled";
       compatible = "neardi,rk356x-adc";
       io-channels = <&saradc x>; // x为通道编号，申请 SARADC 通道0 则 x 为 0
   };
```
这里申请的是 SARADC 通道2
#### 在驱动文件中匹配 DTS 节点

用户驱动可参考 Neardi adc demo :`kernel/drivers/iio/adc/adc-neardi-demo.c`，这是一个侦测 LKD3568风扇状态的驱动。首先在驱动文件中定义 `of_device_id` 结构体数组：

```shell
static const struct of_device_id neardi_adc_match[] = {
     { .compatible = "neardi,rk356x-adc" },
     {},
};
```
然后将该结构体数组填充到要使用 ADC 的 platform_driver 中：
```shell
static struct platform_driver neardi_adc_driver = {
    .probe      = neardi_adc_probe,
    .remove     = neardi_adc_remove,
    .driver     = {
        .name   = "neardi_adc",
        .owner  = THIS_MODULE,
        .of_match_table = neardi_adc_match,
        },
};
```
接着在 neardi_adc_probe 中对 DTS 所添加的资源进行解析：
```shell
static int neardi_adc_probe(struct platform_device *pdev)
{
     printk("neardi_adc_probe!\n");
     chan = iio_channel_get(&(pdev->dev), NULL);
     if (IS_ERR(chan)){
	    chan = NULL;
        printk("%s() have not set adc chan\n", __FUNCTION__);
        return -1;
     }
     fan_insert = false;
     if (chan) {
		INIT_DELAYED_WORK(&adc_poll_work, neardi_demo_adc_poll);
		schedule_delayed_work(&adc_poll_work,1000);
     }
     return 0;
}
```
### 驱动说明

#### 获取 AD 通道

```shell
struct iio_channel *chan;     #定义 IIO 通道结构体
chan = iio_channel_get(&pdev->dev, NULL);    #获取 IIO 通道结构体
```
**注意：** `iio_channel_get` 通过 probe 函数传进来的参数 pdev 获取 IIO 通道结构体，probe 函数如下：
```shell
static int XXX_probe(struct platform_device *pdev);
```
### 读取 AD 采集到的原始数据

```shell
int val,ret;
ret = iio_read_channel_raw(chan, &val);
```
调用 iio_read_channel_raw 函数读取 AD 采集的原始数据并存入 val 中。
### 计算采集到的电压

使用标准电压将 AD 转换的值转换为用户所需要的电压值。其计算公式如下：

```shell
Vref / (2^n-1) = Vresult / raw
```
注意：
* Vref 为标准电压
* n 为 AD 转换的位数
* Vresult 为用户所需要的采集电压
* raw 为 AD 采集的原始数据
例如，标准电压为 1.8V，AD 采集位数为 10 位，AD 采集到的原始数据为 568，则：

```shell
Vresult = (1800mv * 568) / 1023;
```
### 接口说明

```shell
struct iio_channel *iio_channel_get(struct device *dev, const char *consumer_channel);
```
* 功能：获取 iio 通道描述
* 参数：
    * dev: 使用该通道的设备描述指针
    * consumer_channel: 该设备所使用的 IIO 通道描述指针
```shell
void iio_channel_release(struct iio_channel *chan);
```
* 功能：释放 iio_channel_get 函数获取到的通道
* 参数：
    * chan：要被释放的通道描述指针
```shell
int iio_read_channel_raw(struct iio_channel *chan, int *val);
```
* 功能：读取 chan 通道 AD 采集的原始数据。
* 参数：
    * chan：要读取的采集通道指针
    * val：存放读取结果的指针
### 调试方法

#### Demo 程序使用

在 DTS 中使能 adc_demo ，将 `disabled` 改为 `okay`:

```shell
adc_demo: adc_demo{
        status = "okay";
        compatible = "neardi,rk356x-adc";
        io-channels = <&saradc 5>;
    };
```
编译内核，烧录内核到 LKD3568 开发板上，然后插拔风扇时，会打印内核 log 信息如下：
```shell
[   85.158104] Fan insert! raw= 135 Voltage= 237mV
[   88.422124] Fan out! raw= 709 Voltage=1247mV
```
#### 获取所有 ADC 值

有个便捷的方法可以查询到每个 SARADC 的值：

```shell
cat /sys/bus/iio/devices/iio\:device0/in_voltage*_raw
```

 

