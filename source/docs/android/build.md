# Android11系统编译


## 硬件配置

编译 Android 11 开发环境硬件配置建议：

- 64 位 CPU
- Ubuntu20.04系统
- 16GB  内存
- 250GB  空闲空间用来编译
- 普通用户编译，不要使用 root 用户编译

另外可参考 Google 官方文档硬件和软件配置：

- [https://source.android.com/setup/build/requirements](https://source.android.com/setup/build/requirements)
- [https://source.android.com/setup/initializing](https://source.android.com/setup/initializing)

## 软件配置

**安装环境包**

``` shell
sudo apt update
sudo apt-get install git-core gnupg flex bison build-essential zip curl \
  zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 libncurses5 lib32ncurses5-dev \
  x11proto-core-dev libx11-dev lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip \
  fontconfig liblz4-tool libxml2-utils libssl-dev
```

**SDK获取**

发送邮件至 [support@neardi.com](support@neardi.com) 获取相关开发资料

下载完成后，在解压前先查看 MD5 码和LKD3568-SDK-Android-V1.0.tar.gz.md5sum中是否一致：

``` shell
$ md5sum LKD3568-SDK-Android-V1.0-split.tar.gz*
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzaa
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzab
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzac
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzad
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzae
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzaf
********************************  LKD3568-SDK-Android-V1.0-split.tar.gzag
``` 

合并解压：

``` shell
$ cat LKD3568-SDK-Android-V1.0-split.tar.gz* > LKD3568-SDK-Android-V1.0.tar.gz
$ tar -zxf LKD3568-SDK-Android-V1.0.tar.gz
``` 

提取：

``` shell
cd LKD3568-SDK-Android-V1.0
git reset --hard
```

切换分支：

|branch|explain|
|:----|:----|
|lz12000003|Neardi master branch|

``` shell
git checkout lz12000003
``` 

## 编译固件

**编译 U-Boot**

``` shell
./build.sh -U
``` 

**编译内核**

``` shell
./build.sh -K
``` 

**编译 Android**

``` shell
make installclean
./build.sh -A
``` 

## 打包固件

编译完成后, 将所有的分区映像打包成RK整包固件格式用于烧录：

``` shell
./build.sh -u
``` 

## 全自动编译
全自动编译会执行所有编译、打包操作，直接生成 RK 固件。

``` shell
./build.sh -AKUpu
``` 

该脚本会将编译生成的固件拷贝至：rockdev/Image-rk3568_r/目录下，具体路径以实际生成为准。每次编译都会新建目录保存，自动备份调试开发过程的固件版本，并存放固件版本的各类信息。该目录下的update.img可直接用于Android开发工具及工厂烧写工具下载更新。


## 分区映像

`update.img` 是发布给最终用户的固件，方便升级开发板。而在实际开发中，更多的时候是修改并烧写单个分区映像文件，这样做大大节省开发时间。

如下是映像文件列表：

- `boot.img`： Android 的 initramfs 映像，包含Android根目录的基础文件系统，它负责初始化和加载系统分区。
- `system.img`： ext4 文件系统格式的 Android 文件系统分区映像。
- `kernel.img`： 内核映像。
- `resource.img`： Resource 映像， 包含启动图片和内核设备树。
- `misc.img`： misc 分区映像， 负责启动模式的切换和急救模式参数的传递。
- `recovery.img`： Recovery 模式映像。
- `rk3568_loader_xxx.bin`： Loader 文件。
- `uboot.img`： U-Boot 映像文件。
- `trust.img`： Arm trusted file (ATF) 映像文件。
- `parameter.txt`： 分区布局和内核命令行。

