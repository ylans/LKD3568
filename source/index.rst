.. LKD3568 Board documentation master file.

LKD3568 用户手册
=====================================

.. toctree::
   :maxdepth: 3
   :caption: LKD3568

   docs/preface

.. toctree::
   :maxdepth: 3
   :caption: 快速入门

   docs/hardware/specs
   docs/hardware/interface
   docs/debug

.. toctree::
   :maxdepth: 3
   :caption: 升级固件

   docs/boot_mode
   docs/upgrade
   docs/maskrom_mode

.. toctree::
   :maxdepth: 3
   :caption: Android开发

   docs/android/build

.. toctree::
   :maxdepth: 2
   :caption: Linux开发

   docs/linux/ubuntu
   docs/linux/debian
   docs/linux/buildroot
   docs/linux/iodomain

.. toctree::
   :maxdepth: 2
   :caption: 内核驱动

   docs/linux/hdmiin
   docs/linux/gpio
   docs/linux/rtc
   docs/linux/uart
   docs/linux/can
   docs/linux/lcd
   docs/linux/ethernet

.. toctree::
   :maxdepth: 3
   :caption: Demo

   docs/demo/demo_android
   docs/demo/demo_linux

.. toctree::
   :maxdepth: 3
   :caption: 系统定制

   docs/android_custom
   docs/ubuntu_custom

.. toctree::
   :maxdepth: 3
   :caption: 常见问题

   docs/faq

.. _notes:

.. toctree::
    :caption: Release Notes

    docs/notes
